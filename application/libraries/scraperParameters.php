<?php
    class scraperParameters
    {
        public function getScraperParameters()
        {
            $parameters = array();
            array_push($parameters,array("ftp","https://webas63524.tld.pl/Ftp/Index/page/",array("name","description","php","expires-at","blocked-ftp","blocked-www","blocked-mail","domains","quota","quota-percent","limit")));
            array_push($parameters,array("domains","https://webas63524.tld.pl/Domain/Index/page/",array("domain","ftp","directory","php","ssl-protection","dkim-active","cdn","blocked-www","blocked-mail","sub","email","quota")));
            array_push($parameters,array("dns","https://webas63524.tld.pl/Dns/Index/page/",array("domain","ftp","domain_ip","domain_mx","cleaner_date")));
            array_push($parameters,array("mailboxes","https://webas63524.tld.pl/EmailAccount/Index/page/",array("name","description","blocked","usage","quota")));

            return $parameters;
        }
    }
?>
