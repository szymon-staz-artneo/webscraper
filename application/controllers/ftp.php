<?php
defined('BASEPATH') OR exit('No direct script access allowed');
    class ftp extends CI_Controller{
        
        public function __construct()
        {
            parent::__construct();
            
            $this->load->model("scraper");
        }

        public function index(){
            $this->scraper->index();
        }

        public function getFTPData(){
            return $this->scraper->returnDataFromDBTable("ftp");
        }

        public function getDNSData(){
            return $this->scraper->returnDataFromDBTable("dns");
        }

        public function getDomainsData(){
            return $this->scraper->returnDataFromDBTable("domains");
        }

        public function getMailboxesData(){
            return $this->scraper->returnDataFromDBTable("mailboxes");
        }
    }


?>