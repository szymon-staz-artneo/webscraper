<?php
    class scraper extends CI_Model{
        public function __construct()
        {
            $this->load->database();
            $this->load->library('simple_html_dom');
            $this->load->library('login_data');
            $this->load->library('scraperParameters');
            $this->login_data->get_authorization_data();
        }

        private function login(){
            $data = array(
                'login'=>$_SESSION['login'],
                'password'=>$_SESSION['password'],
                "stay_signed"=> "0",
                "stay_signed"=> "1"
            );
            $ch = curl_init($_SESSION["login_site"]);
            
            curl_setopt($ch,CURLOPT_POST,1);
            curl_setopt($ch,CURLOPT_POSTFIELDS,"login=admin&password=dosuszmy%7E8SmQ%2FK%3A1%23h%23f&stay_signed=0");
            curl_setopt($ch, CURLOPT_COOKIESESSION, true );
            curl_setopt($ch,CURLOPT_COOKIEJAR,".cookie.txt");
            curl_setopt($ch, CURLOPT_COOKIEFILE, ".cookie.txt" );
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
            $result = curl_exec($ch);
            curl_close($ch);
        }

        private function removeBull($data){
            $returnArray = array();
            $counter = 0;
            foreach($data as $x){
                if(mb_convert_encoding($x,"UTF-8")=="&bull;"){
                    array_push($returnArray,"bull");
                }else{
                    array_push($returnArray,$x);
                }
                $counter++;
            }
            return $returnArray;
        }

        private function scrapSinglePage($page){
            $ft = curl_init($page);
            curl_setopt($ft, CURLOPT_COOKIESESSION, true );
            curl_setopt($ft,CURLOPT_COOKIEJAR,".cookie.txt");
            curl_setopt($ft, CURLOPT_COOKIEFILE, ".cookie.txt" );
            curl_setopt($ft, CURLOPT_RETURNTRANSFER, true );
            curl_setopt($ft, CURLOPT_AUTOREFERER, true );
            curl_setopt($ft, CURLOPT_FOLLOWLOCATION, true );
            curl_setopt($ft, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($ft, CURLOPT_SSL_VERIFYPEER, FALSE);
            $result = curl_exec($ft);
            curl_close($ft);
            return $result;
        }

        private function readHeaders($htmlPage){
            $html = new simple_html_dom();
            $html->load($htmlPage, true, false);
            $table=$html->find('thead',0);
            if($table==null){
                return false;
            }
            $dataArray = array();
            foreach($table->find("th") as $x){
                array_push($dataArray,$x->id);
            }
            return $dataArray;
        }

        private function readScrappedPage($htmlPage,$allowedColumns,$columns){
            $html = new simple_html_dom();
            $html->load($htmlPage, true, false);
            $table=$html->find('tbody',0);
            if($table==null){
                return false;
            }
            $counter=0;
            $counterRow=0;
            $dataArray = array();
            foreach($table->find('tr') as $tr){
                $dataArray[$counterRow] = array();
                foreach($tr->find('td') as $td){
                    if(!in_array($columns[$counter],$allowedColumns)){
                        $counter++;
                        continue;
                    }
                    if($td->find('span.tag-yes')){
                        array_push($dataArray[$counterRow],"true");
                    }else if($td->find('span.tag-no')){
                        array_push($dataArray[$counterRow],"false");
                    }else{
                        array_push($dataArray[$counterRow],$td->plaintext);
                    }
                    $counter++;
                }
                $counter=0;
                $counterRow++;
            }
            unset($html);
            return $dataArray;
        }

        private function scrapWholePage($baseUrl,$excludedColumns){
            $wholeArray = array();
            $counter=0;
            $htmlHeaders=array();
            while(true){
                $result=$this->scrapSinglePage($baseUrl.$counter);
                if($counter==0){
                    $htmlHeaders = $this->readHeaders($result);
                }
                if($htmlHeaders==false){
                    break;
                }
                $htmlArray = $this->readScrappedPage($result,$excludedColumns,$htmlHeaders);
                if($htmlArray==false){
                    break;
                }
                foreach($htmlArray as $x){
                    array_push($wholeArray,$this->removeBull($x));
                }
                $counter++;
            }

            $readyArray = array();

            foreach($wholeArray as $x){
                array_push($readyArray,$this->preparaDataForInsert($x,$htmlHeaders,$excludedColumns));
            }

            return $readyArray;
        }

        private function getRecordFromTable($record,$tableName){
            $key = array_keys($record)[0];
            $keyValue = "";
            foreach($record as $x){
                $keyValue=$x;
                break;
            }
            $this->db
                ->from($tableName)
                ->where($key,$keyValue);

            $query = $this->db->get();
            $result = $query->result_array();
            return $result;
        }

        private function insertRecord($data,$table){
            foreach($data as $x){
                $keyValue=$x;
                break;
            }
            $this->db->insert($table, $data);
        }

        private function updateRecord($data,$table){
            $key = array_keys($data)[0];
            $keyValue = "";

            foreach($data as $x){
                $keyValue=$x;
                break;
            }
            $this->db
                ->where($key,$keyValue)
                ->update($table,$data);
        }

        private function CheckIfRecordDiffers($scrapedRecord,$dbRecord){
            $counter=0;
            foreach($dbRecord[0] as $key => $value){
                if($value!=$scrapedRecord[$key]){
                    return true;
                }
                $counter++;
            }
            return false;
        }

        private function preparaDataForInsert($data,$columns,$allowedColumns){
            $insertData=array();
            $counter=0;
            $counterColumn=0;
            foreach($columns as $x){
                if(!in_array($x,$allowedColumns)){
                    $counterColumn++;
                    continue;
                }
                $insertData[$columns[$counterColumn]] = $data[$counter];
                $counterColumn++;
                $counter++;
            }

            return $insertData;
        }

        private function syncRecord($record,$table){
                $dbRecord = $this->getRecordFromTable($record,$table);
                
                if(count($dbRecord)==0){
                    $this->insertRecord($record,$table);
                }
                else if($this->CheckIfRecordDiffers($record,$dbRecord)){
                    $this->updateRecord($record,$table);
                }

        }

        private function Sync($tableName,$baseUrl,$allowedColumns){
            $scraperdArray = $this->scrapWholePage($baseUrl,$allowedColumns);
            foreach($scraperdArray as $x){
                $this->syncRecord($x,$tableName);
            }
        }

        public function returnDataFromDBTable($tableName){
            $query=$this->db->get($tableName);
            $result = $query->result_array();
            
            $json= json_encode($result);
            echo($json);
            return $json;
        }

        public function index(){
            $this->login();
            $parameters = $this->scraperparameters->getScraperParameters();
            foreach($parameters as $parameter){
                $this->Sync($parameter[0],$parameter[1],$parameter[2]);
            }
        }
    }
?>